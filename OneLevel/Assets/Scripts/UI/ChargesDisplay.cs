using System;
using System.Collections;
using System.Collections.Generic;
using Mechanics;
using TMPro;
using UnityEngine;

public class ChargesDisplay : MonoBehaviour
{
	[SerializeField] protected TextMeshProUGUI ChargeDisplayText;
	[SerializeField] protected WholeCharges dashWholeCharges;
	[SerializeField] protected GameObject baseCircle;
	[SerializeField] protected Transform parentCharges;

	private int _activeCircles;
	private int _maxCircles;
	private readonly List<GameObject> _circles = new List<GameObject>();
	private void Awake()
	{
		dashWholeCharges.RechargedAction += OnDashRecharged;
		dashWholeCharges.ChargeUsedAction += OnChargeUse;
		_maxCircles = dashWholeCharges.ChargesMax;
	}

	private void Start()
	{
		// Set the current charge count to our text
		// hint: ChargeDisplayText.text = ...
		InitializeChargeObjects();
	}

	private void OnDashRecharged(int currentCharges)
	{
		_activeCircles = Mathf.FloorToInt(currentCharges);
		// This gets called when we get a charge back
		// Update current charge count text
		UpdateChargeDisplay();
	}

	private void OnChargeUse()
	{
		_activeCircles = dashWholeCharges.CurrentCharges;
		UpdateChargeDisplay();
	}

	private void InitializeChargeObjects()
	{
		for(int count = 0; count < _maxCircles; count++)
		{
			GameObject tempCircle = Instantiate(baseCircle, parentCharges);
			_circles.Add(tempCircle);	
		}
	}

	private void UpdateChargeDisplay()
	{
		for (int i = 0; i < _circles.Count; i++)
		{
			_circles[i].SetActive(i <= _activeCircles);
		}
	}
}
