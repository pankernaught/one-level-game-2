using System;
using System.Collections;
using System.Collections.Generic;
using Movement;
using TMPro;
using UnityEngine;

public class MoveStateDisplay : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI moveStateText;
    [SerializeField] protected MovementAbilities moveStateMachinel;

    private void Start()
    {
        moveStateMachinel.MoveStateChangedAction += OnMoveStateChanged;
    }

    private void OnMoveStateChanged(EMovementState state)
    {
        moveStateText.text = state.ToString().ToUpper();
    }
}
