using System;
using System.Collections.Generic;
using Mechanics;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class StaminaDisplay : MonoBehaviour
    {
        [SerializeField] protected List<Image> staminaBarImages;
        [SerializeField] protected ContinuousCharges staminaCharges;

        private float _maxChage;
        
        private void Awake()
        {
            _maxChage = staminaCharges.ChargesMax;
            staminaCharges.ChargeChangedAction += OnStaminaChanged;
        }

        private void OnStaminaChanged(float value)
        {
            foreach (var barImage in staminaBarImages)
            {
                barImage.fillAmount = value / _maxChage;
            }
        }
    }
}