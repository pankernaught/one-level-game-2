using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class GroovyCanvas : MonoBehaviour
{
    [Range(0.01f, 5f)]
    [SerializeField] protected float FlashTime = 1f;

    protected Image canvasImage;
    private Color color;

    private void Awake() {
        canvasImage = gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Start()
    {
        StartCoroutine(ColorFun());
    }

    private IEnumerator ColorFun()
    {
        do
        {
            RandomizeColor();
            canvasImage.color = this.color;
            yield return new WaitForSeconds(FlashTime);
        } while (true);
    }

    private void RandomizeColor(){
        color.r = Random.value;
        color.g = Random.value;
        color.b = Random.value;
        color.a = 1f;
    }
}
