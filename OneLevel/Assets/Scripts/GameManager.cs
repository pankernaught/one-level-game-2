using System;
using System.Collections.Generic;
using System.Linq;
using Interfaces;
using UnityEngine;
using UnityEngine.InputSystem;


// Game manager manages state of the game
public class GameManager : MonoBehaviour
{
	public static GameManager Instance { get; private set; } = null;

	public Action<EGameModeType> GameModeTypeChangedAction;
	
	private readonly List<MonoBehaviour> _isometricBehaviours = new List<MonoBehaviour>();
	private readonly List<MonoBehaviour> _twoDBehaviours = new List<MonoBehaviour>();

	private EGameModeType _currentEGameModeType = EGameModeType.Isometric;

	private void Awake()
	{
		Instance = this;

		var implementsInterface = FindObjectsOfType<MonoBehaviour>(true).OfType<IGameModeTypeable>().ToArray();
		foreach (var _interface in implementsInterface)
		{
			if (_interface.GetGameModeType() == EGameModeType.Isometric)
			{
				_isometricBehaviours.Add(_interface.GetMonoBehaviour());
			}
			else if (_interface.GetGameModeType() == EGameModeType.TwoD)
			{
				_twoDBehaviours.Add(_interface.GetMonoBehaviour());
			}
		}
	}

	private void Start()
	{
		SetGameModeType(_currentEGameModeType);
	}

	public void SetGameModeType(EGameModeType eGameModeType)
	{
		switch (eGameModeType)
		{
			case EGameModeType.Isometric:
				SetListEnabledState(_isometricBehaviours, true);
				SetListEnabledState(_twoDBehaviours, false);
				break;

			case EGameModeType.TwoD:
				SetListEnabledState(_isometricBehaviours, false);
				SetListEnabledState(_twoDBehaviours, true);
				break;

			case EGameModeType.None:
			default:
				throw new ArgumentOutOfRangeException(nameof(eGameModeType), eGameModeType, "Game Type Mode Not Supported");
		}
		
		GameModeTypeChangedAction?.Invoke(eGameModeType);
	}

	private static void SetListEnabledState(List<MonoBehaviour> list, bool enabledState)
	{
		foreach (var thing in list)
		{
			thing.enabled = enabledState;
		}
	}

	private void OnChangeCamera(InputValue inputValue)
	{
		if (!inputValue.isPressed) return;
		
		switch (_currentEGameModeType)
		{
			case EGameModeType.Isometric:
				_currentEGameModeType = EGameModeType.TwoD;
				break;

			case EGameModeType.TwoD:
				_currentEGameModeType = EGameModeType.Isometric;
				break;
			
			case EGameModeType.None:
			default:
				throw new ArgumentOutOfRangeException();
		}

		SetGameModeType(_currentEGameModeType);
	}
}
