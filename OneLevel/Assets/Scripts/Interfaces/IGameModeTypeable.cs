using UnityEngine;

namespace Interfaces
{
	public interface IGameModeTypeable
	{
		EGameModeType GetGameModeType();
		MonoBehaviour GetMonoBehaviour();
	}
}
