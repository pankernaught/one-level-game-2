using System;
using Interfaces;
using UnityEngine;

namespace Camera
{
	public class FollowCameraBase : MonoBehaviour, IGameModeTypeable
	{
		[SerializeField] protected Transform focusTransform;
		[SerializeField] protected float distance;
		[SerializeField] protected float pitch;
		[SerializeField] protected float yaw;
		[SerializeField] protected AnimationCurve followCurve;

		// The location that the camera wants to be at
		protected UnityEngine.Camera FollowCamera;

		private void Awake()
		{
			FollowCamera = GetComponentInChildren<UnityEngine.Camera>();
			// Do the cacluations for where the camera should be using the Focus Transform
			SetTargetPosition();
		}

		private void FixedUpdate()
		{
			// Calculate follow strength
			//Move to Target
			float distance = Vector3.Distance(focusTransform.position, transform.position);
			if (distance > 0.01f)
			{
				float eval = followCurve.Evaluate(distance);
				transform.position = Vector3.Lerp(transform.position, focusTransform.position, eval);
			}
		}
		protected virtual void SetTargetPosition()
		{
			throw new NotImplementedException();
		}

		public virtual EGameModeType GetGameModeType()
		{
			return EGameModeType.None;
		}

		public virtual MonoBehaviour GetMonoBehaviour()
		{
			return this;
		}
	}
}
