using UnityEngine;

namespace Camera
{
	public class TwoDCameraFollow : FollowCameraBase
	{
		protected override void SetTargetPosition()
		{
			// Set camera rotation
			transform.rotation = Quaternion.Euler(0f, yaw, 0f);
			
			// Calculate camera target location
			//TargetPosition = focusTransform.position + -transform.forward * distance;
		}

		public override EGameModeType GetGameModeType()
		{
			return EGameModeType.TwoD;
		}

		public override MonoBehaviour GetMonoBehaviour()
		{
			return this;
		}
	}
}
