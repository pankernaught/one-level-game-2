using System;
using DG.Tweening;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Camera
{
    public class CameraSwivel : MonoBehaviour
    {
        public static CameraSwivel Instance { get; private set; } = null;
        
        [SerializeField] protected float isoYaw = 45f;
        private float swivelYaw = 0f;
        
        public float Yaw => isoYaw + swivelYaw;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            var final = Quaternion.Euler(0f, 45f, 0f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, final, 180f);
        }

        private void OnSwivelCamera(InputValue inputValue)
        {
            float value = inputValue.Get<float>();
            if (value > 0)
            {
                // Swivel Right
                swivelYaw -= 90f;
            }
            else
            {
                // Swivel Left
                swivelYaw += 90f;
            }

            var final = Quaternion.Euler(0f, Yaw, 0f);
            transform.DORotate(final.eulerAngles, 0.25f);
        }
    }
}