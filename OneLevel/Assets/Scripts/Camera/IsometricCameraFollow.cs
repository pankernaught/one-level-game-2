using Interfaces;
using UnityEngine;

namespace Camera
{
	public class IsometricCameraFollow : FollowCameraBase, IGameModeTypeable
	{
		protected override void SetTargetPosition()
		{
			//Set Camera Rotation
			FollowCamera.transform.localRotation = Quaternion.Euler(pitch, 0f, 0f);
			// Calculate camera target location
			var camPos = -FollowCamera.transform.forward * distance;
			FollowCamera.transform.localPosition = camPos;
		}

		public override EGameModeType GetGameModeType()
		{
			return EGameModeType.Isometric;
		}

		public override MonoBehaviour GetMonoBehaviour()
		{
			return this;
		}
	}
}
