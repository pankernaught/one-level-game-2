using System;
using Movement.Movement_Abilities;
using Unity.VisualScripting.FullSerializer.Internal.Converters;
using UnityEngine;
using Utils;

namespace Movement
{
    public class MovementAbilities : MovementBase
    {
        [SerializeField] protected bool canJump = true;
        [SerializeField] protected bool canJumpExtend = true;
        [SerializeField] protected bool canDash = true;
        [SerializeField] protected bool canCrouch = true;
        [SerializeField] protected bool canSprint = true;
        
        public Action<EMovementState> MoveStateChangedAction;
        
        private Jump _jump;
        private Dash _dash;
        private Crouch _crouch;
        private Sprint _sprint;

        private readonly StateMachine<EMovementState> _movementStateMachine = new StateMachine<EMovementState>();

        protected override void Awake()
        {
            base.Awake();
            
            _jump = GetComponent<Jump>();
            _dash = GetComponent<Dash>();
            _crouch = GetComponent<Crouch>();
            _sprint = GetComponent<Sprint>();
        }

        protected void Start()
        {
            // Register for Action
            _movementStateMachine.MoveStateChangedAction += OnStateChanged;

            #region Idle State

            //// Idle State ////
            _movementStateMachine.AddState(EMovementState.Idle, EStateBehaviour.OnlyOnce, EStateType.Single, EStateLocation.Internal,
                () => { },
                () => enabled && _characterController.isGrounded, 
                (from, to) => { }, 
                (from, to) => { });
            // Enter Idle State Trigger
            StoppedMovingAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Idle);
            StoppedInputAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Idle);
            
            #endregion

            #region Walking State

            //// Walking State ////
            _movementStateMachine.AddState(EMovementState.Walking, EStateBehaviour.OnlyOnce, EStateType.Single, EStateLocation.Internal,
                () => { },
                () => enabled && _characterController.isGrounded,
                (from, to) => { },
                (from, to) => { });
            // Enter Walking State Trigger 
            StartedMovingActon += () => _movementStateMachine.ChangeMovementState(EMovementState.Walking);
            BecameGroundedAction += () =>
            {
                if (_movementStateMachine.CurrentState() is EMovementState.Falling && !_sprint.IsSprinting() && HasMoveInput && canMove)
                {
                    _movementStateMachine.ChangeMovementState(EMovementState.Walking);
                }
            };
            
            #endregion
            
            #region Falling State

            //// Falling State ////
            _movementStateMachine.AddState(EMovementState.Falling, EStateBehaviour.OnlyOnce, EStateType.Single, EStateLocation.Internal,
                () => {},
                () =>
                {
                    if (!enabled) return false;
                    
                    // Breaks Multi
                    if (_movementStateMachine.CurrentState() is EMovementState.Crouching)
                    {
                        _movementStateMachine.EndMultiState();
                    }

                    return true;
                },
                (from, to) => { },
                (from, to) => { });
            // Enter Falling State Trigger
            StartedFallingAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Falling);

            #endregion
            
            #region Jumping State

            //// Jump State ////
            _movementStateMachine.AddState(EMovementState.Jumping, EStateBehaviour.Repeatable, EStateType.Single, EStateLocation.External,
                () => { /*LastGravitationalVelocity = Vector3.zero;*/ },
                () =>
                {
                    if (!enabled) return false;
                    if (!canJump || !_characterController.isGrounded && !_jump.WithinKyoteTime || _movementStateMachine.CurrentState() is EMovementState.Dashing) return false;
                    // Breaks Multi
                    if (_movementStateMachine.CurrentState() is EMovementState.Crouching && _crouch.CanUnCrouch())
                    {
                        _movementStateMachine.EndMultiState();
                        return true;
                    }
                    return _movementStateMachine.CurrentState() is not EMovementState.Crouching;
                },
                (from, to) => { },
                (from, to) => { });
            // Enter Jump State Triggers
            _jump.JumpAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Jumping);
            // External CanEnterState Conditions
            _jump.CanJumpFunc = () => _movementStateMachine.CanEnterState(EMovementState.Jumping);
            _jump.CanJumpExtendFunc = () => canJumpExtend;

            #endregion
            
            #region Dashing State

            //// Dash State ////
            _movementStateMachine.AddState(EMovementState.Dashing, EStateBehaviour.OnlyOnce, EStateType.Super, EStateLocation.External,
                () => { /*LastGravitationalVelocity = Vector3.zero;*/ },
                () =>
                {
                    if (!enabled) return false;
                    if (!canDash) return false;
                    return !_movementStateMachine.CurrentState().Equals(EMovementState.Crouching) || _crouch.CanUnCrouch();
                },
                (from, to) =>
                {
                    SetCanMove(false);
                    SetUseGravity(false);
                    SetCanSlide(false);
                },
                (from, to) =>
                {
                    SetCanMove(true);
                    SetUseGravity(true);
                    SetCanSlide(true);
                });
            // Enter/Exit Dash State Trigger
            _dash.DashAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Dashing);
            _dash.DashCompleteAction = () => _movementStateMachine.EndSuperState();
            // External CanEnterState Conditions
            _dash.CanDashFunc = () => _movementStateMachine.CanEnterState(EMovementState.Dashing);
            CantMoveInDirectionAction += () => _dash.EndDash();

            #endregion
            
            #region Sprinting State

            //// Sprint State ////
            _movementStateMachine.AddState(EMovementState.Sprinting, EStateBehaviour.OnlyOnce, EStateType.Single, EStateLocation.External,
                () => { },
                () =>
                {
                    if (!enabled) return false;
                    if (!canSprint || !_characterController.isGrounded) return false;
                    if (_movementStateMachine.CurrentState() is EMovementState.Crouching && _crouch.CanUnCrouch())
                    {
                        _movementStateMachine.EndMultiState();
                        return true;
                    }
                    return _movementStateMachine.CurrentState() is not EMovementState.Crouching;
                },
                (from, to) => { },
                (from, to) =>
                {
                    if (to is not (EMovementState.Jumping or EMovementState.Falling or EMovementState.Dashing))
                    {
                        _sprint.ExitSprint();
                    }
                });
            // Enter/Exit String State Trigger
            _sprint.SprintAction += () => _movementStateMachine.ChangeMovementState(EMovementState.Sprinting);
            BecameGroundedAction += () =>
            {
                if (_movementStateMachine.CurrentState() is EMovementState.Falling && _sprint.IsSprinting())
                {
                    _movementStateMachine.ChangeMovementState(EMovementState.Sprinting);
                }
            };
            CantMoveInDirectionAction += () =>
            {
                if (_movementStateMachine.CurrentState() is EMovementState.Falling or EMovementState.Jumping && _sprint.IsSprinting())
                {
                    _sprint.ExitSprint();
                }

                if (_movementStateMachine.CurrentState() is EMovementState.Sprinting)
                {
                    _movementStateMachine.ChangeMovementState(EMovementState.Walking);
                }
            };
            _sprint.SprintCompleteAction += () =>
            {
                // If we let go of sprint and are still sprinting, go to walking
                if (_movementStateMachine.CurrentState() is EMovementState.Sprinting)
                {
                    _movementStateMachine.ChangeMovementState(EMovementState.Walking);
                }
                
                // If we stop sprinting in the air
                if (_movementStateMachine.CurrentState() is EMovementState.Jumping or EMovementState.Falling)
                {
                    _sprint.ExitSprint();
                }
            };
            // External CanEnterState Condition
            _sprint.CanSprint += () => _movementStateMachine.CanEnterState(EMovementState.Sprinting);

            #endregion
            
            #region Crouching State

            //// Crouch State ////
            _movementStateMachine.AddState(EMovementState.Crouching, EStateBehaviour.OnlyOnce, EStateType.Multi, EStateLocation.External,
                () => { },
                () => enabled && canCrouch && _characterController.isGrounded &&
                      _movementStateMachine.CurrentState() is not EMovementState.Dashing,
                (from, to) => _crouch.EnterCrouch(),
                (from, to) => _crouch.ExitCrouch());
            // Enter/Exit Crouch State Trigger
            _crouch.CrouchAction += (crouched) =>
            {
                if (crouched)
                {
                    _movementStateMachine.ChangeMovementState(EMovementState.Crouching);
                }
                else
                {
                    _movementStateMachine.EndMultiState();
                }
            };
            // External CanEnterState Condition
            _crouch.CanCrouchFunc = () => _movementStateMachine.CanEnterState(EMovementState.Crouching);

            #endregion
        }

        private void OnStateChanged(EMovementState state)
        {
            MoveStateChangedAction?.Invoke(state);
        }
    }
}
