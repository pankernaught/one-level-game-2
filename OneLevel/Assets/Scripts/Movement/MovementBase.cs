using System;
using System.Collections.Generic;
using System.Diagnostics;
using Interfaces;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngineInternal;

namespace Movement
{
	public class MovementBase : MonoBehaviour, IGameModeTypeable
	{
		// Direction
		protected virtual Vector3 ForwardDirection => Vector3.forward;
		protected virtual Vector3 RightDirection => Vector3.right;

		[Header("Movement")]
		[SerializeField] protected float moveSpeed;
		[Header("Gravity")]
		[SerializeField] protected Vector3 gravity;

		[SerializeField] protected float maxGravityMagnitude;
		[SerializeField] protected bool useGravity = true;
	
		[Header("Collision Detection")]
		[SerializeField] protected float heightContactLimit = 0.5f;
		[Range(0.1f, 1f)] [SerializeField] protected float floorCastRadiusOffset = 0.5f;
		
		[Header("Flags")] 
		[SerializeField] protected bool canMove = true;

		// Actions
		public Action BecameGroundedAction;
		public Action StoppedInputAction;
		public Action StartedMovingActon;
		public Action StoppedMovingAction;
		public Action CantMoveInDirectionAction;
		public Action StartedFallingAction;
		public Action CanMoveAction;

		// Flags
		protected bool _canForces = true;
		protected bool _canSlide = true;
		private bool _walkableSlope = true;
		private bool _lastUpdateGrounded = false;
		private bool _isMoving = false;
	
		// Movement
		protected float _forwardBackMultiplier = 0f;
		protected float LeftRightMultiplier = 0f;
		private float _slopeAngle = 0f;
		private float _walkSpeed;
	
		// Forces and caches
		protected Vector3 LastTotalMoveVelocity = Vector3.zero;
		protected Vector3 LastGravitationalVelocity = Vector3.zero;
		private Vector3 _gravitationalVelocity = Vector3.zero;
		private Vector3 _slopeDirection = Vector3.zero;
		private Vector3 _slopeCrossProduct = Vector3.zero;
		private Vector3 _slideVelocity = Vector3.zero;
		private Vector3 _forcesVelocity = Vector3.zero;
		private Vector3 _moveDirectionCache = Vector3.zero;
		

		private readonly List<Vector3> _wallCollisionNormals = new List<Vector3>();

		// Component References
		protected CharacterController _characterController;

		// Constants
		private const int RotationSubDivisions = 12;
		private Vector3 MinGravityVector3 => gravity * 0.1f;

		// Collisions
		private RaycastHit _hitInfo;

		// Getters & Setters
		protected bool HasMoveInput => _forwardBackMultiplier != 0f || LeftRightMultiplier != 0f;
		public bool IsWalkableSlope => _walkableSlope;
		public bool IsFalling => _gravitationalVelocity.y < 0f;
		public Vector3 GetGravity => gravity;
		public float MoveSpeed
		{
			get => moveSpeed;
			set => moveSpeed = value;
		}

		/// <summary>
		/// The current direction of planar movement
		/// </summary>
		protected virtual Vector3 GetMoveDirectionFromInput => ((_forwardBackMultiplier * ForwardDirection) + (LeftRightMultiplier * RightDirection)).normalized;

		public Vector3 GetMoveDirection
		{
			get {
				// Flip he slope angle based on if we are moving up or down the slope
				float slopeAngle = Vector3.Dot(_moveDirectionCache, _slopeDirection) > 0 ? -_slopeAngle : _slopeAngle;
				// Rotate moveDirection around the CrossProduct of the slope by the slope angle
				return Quaternion.AngleAxis(slopeAngle, _slopeCrossProduct) * _moveDirectionCache;
			}
		}

		public bool MovingUp => _forwardBackMultiplier > 0f;

		public void SetCanMove(bool canMove)
		{
			if (!this.canMove && canMove)
			{
				CanMoveAction?.Invoke();
			}
			
			this.canMove = canMove;
		}

		public void SetUseGravity(bool active)
		{
			useGravity = active;
			if (!useGravity)
			{
				_gravitationalVelocity = MinGravityVector3;
			}
		}

		public void SetCanSlide(bool active)
		{
			_canSlide = active;
		}

		// Unity Functions
		protected virtual void Awake()
		{
			_characterController = GetComponent<CharacterController>();

			_walkSpeed = moveSpeed;
		}

		protected virtual void FixedUpdate()
		{
			CalculateSlopeVelocity();

			// Add Move Force
			Move();
		}

		// Public Functions
		public void AddVerticalVelocity(float velocity, ForceMode mode)
		{
			switch (mode)
			{
				case ForceMode.Force:
					break;
			
				case ForceMode.Acceleration:
					break;
			
				case ForceMode.Impulse:
					_gravitationalVelocity.y += velocity;
					break;
			
				case ForceMode.VelocityChange:
					_gravitationalVelocity.y = velocity;
					break;
			
				default:
					throw new ArgumentOutOfRangeException(nameof(mode), mode, "Mode Type not supporter");
			}
		}

		public void AddForce(Vector3 force, ForceMode mode)
		{
			switch (mode)
			{
				case ForceMode.Force:
					break;
			
				case ForceMode.Acceleration:
					break;
			
				case ForceMode.Impulse:
					break;
			
				case ForceMode.VelocityChange:
					_forcesVelocity = force;
					break;
			
				default:
					throw new ArgumentOutOfRangeException(nameof(mode), mode, "Mode Type not supporter");
			}
		}

		public void ResetMoveSpeed()
		{
			MoveSpeed = _walkSpeed;
		}
	
		// Internal Functions, Protected & Private
		private void CalculateSlopeVelocity()
		{
			float groundedMinDistance = _characterController.height * heightContactLimit + _characterController.skinWidth;
			//LayerMask mask = LayerMask.GetMask("Landable");

			// if (Physics.SphereCast(transform.position + _characterController.center, _characterController.radius * FLOOR_CAST_RADIUS_OFFSET, Vector3.down, out _hitInfo,
			//	    _characterController.height * 1.5f, mask))
			if (RadialRayCast(out _hitInfo))
			{
				// check that we are under the slope limit
				_slopeAngle = Vector3.Angle(_hitInfo.normal, Vector3.up);
				_slopeCrossProduct = Vector3.Cross(_hitInfo.normal, Vector3.up).normalized;
				_slopeDirection = Quaternion.AngleAxis(90f, _hitInfo.normal) * _slopeCrossProduct;
				
				float hitDistance = _hitInfo.distance;
				if (_slopeAngle > _characterController.slopeLimit && hitDistance <= groundedMinDistance)
				{
					//Cache for Dash and other move mechanics to use when moving up a slope
					_walkableSlope = false;

					// We are standing on a surface that is too steep to move along so slide down it
					// Calculate Slope Slide Direction And resulting Velocity
					Vector3 slideVelocity = Vector3.Dot(_slopeDirection, Vector3.down) * _slopeDirection * gravity.magnitude;
					
					if (_slideVelocity == Vector3.zero)
					{
						// Set initial velocity to maintain current fall speed
						_slideVelocity += Vector3.Dot(_slopeDirection, Vector3.down) * _slopeDirection * LastGravitationalVelocity.magnitude;
					}
					
					_slideVelocity += slideVelocity * Time.deltaTime;
				}
				else
				{
					_slideVelocity = Vector3.zero;
					_walkableSlope = true;
				}
			}
			else
			{
				_slopeAngle = 0f;
			}
		}
		
		private bool RadialRayCast(out RaycastHit hitInfo)
		{
			hitInfo = new RaycastHit();
		
			LayerMask mask = LayerMask.GetMask("Landable");
			for (int i = 0; i < RotationSubDivisions; i++)
			{
				Vector3 start = transform.position + Quaternion.AngleAxis(Mathf.FloorToInt(360f/RotationSubDivisions) * i, Vector3.down) * (transform.forward * _characterController.radius * floorCastRadiusOffset);

				RaycastHit hit;
				if (Physics.Raycast(start, Vector3.down, out hit, _characterController.height, mask))
				{
					if (hitInfo.collider == null || hit.distance < hitInfo.distance)
					{
						hitInfo = hit;
					}
				}
			}

			return hitInfo.collider != null;
		}

		protected virtual void Move()
		{
			// Apply Gravity
			if (_characterController.isGrounded)
			{
				if (!_lastUpdateGrounded)
				{
					LastGravitationalVelocity = _gravitationalVelocity;
					_gravitationalVelocity = MinGravityVector3;

					// Invoke action to announce that we are grounded
					BecameGroundedAction?.Invoke();
				}
			}
			else if (useGravity)
			{
				// Started falling by dropping off the edge
				if (_gravitationalVelocity == MinGravityVector3)
				{
					StartedFallingAction?.Invoke();
				}

				_gravitationalVelocity += gravity * Time.fixedDeltaTime;
				if (_gravitationalVelocity.magnitude > maxGravityMagnitude)
				{
					_gravitationalVelocity = _gravitationalVelocity.normalized * maxGravityMagnitude;
				}
				
				// Started falling after a jump
				if (_gravitationalVelocity.y < 0f && LastGravitationalVelocity.y >= 0f)
				{
					StartedFallingAction?.Invoke();
				}
				
				LastGravitationalVelocity = _gravitationalVelocity;
			}

			// Update Grounded
			_lastUpdateGrounded = _characterController.isGrounded;

			if (canMove)
			{
				Vector3 move = GetMoveDirectionFromInput * moveSpeed;
				if (CanMoveInDirection(move))
				{
					// XZ Plane Movement
					Vector3 moveDirection = GetMoveDirectionFromInput;
					if (moveDirection != Vector3.zero)
					{
						// Store last move input direction
						_moveDirectionCache = moveDirection;

						if (!_isMoving)
						{
							StartedMovingActon?.Invoke();
						}
						_isMoving = true;
					}
					else if (_isMoving)
					{
						// Invoke action when we stop moving
						_isMoving = false;
						StoppedInputAction?.Invoke();
					}
				}
				else
				{
					CantMoveInDirectionAction?.Invoke();
				}
			}
			
			float gravityFactor = useGravity ? 1f : 0f;
			Vector3 downVelocity = _slideVelocity == Vector3.zero
				? gravityFactor * _gravitationalVelocity
				: _canSlide
					? _slideVelocity
					: Vector3.zero;

			Vector3 moveVelocity = canMove && _isMoving ? GetMoveDirectionFromInput * moveSpeed : Vector3.zero;
			Vector3 forceVelocity = _canForces ? _forcesVelocity : Vector3.zero;
			Vector3 totalMoveVelocity = downVelocity + forceVelocity + moveVelocity;
			Vector3 totalMoveDeltaTime = totalMoveVelocity * Time.fixedDeltaTime;

			
			// Clear Collisions from last Move
			_wallCollisionNormals.Clear();
			
			_characterController.Move(totalMoveDeltaTime);

			if (totalMoveVelocity == MinGravityVector3 && LastTotalMoveVelocity.magnitude > MinGravityVector3.magnitude)
			{
				// We have come to a total stop
				StoppedMovingAction?.Invoke();
			}

			LastTotalMoveVelocity = totalMoveVelocity;
		}

		public bool CanMoveInDirection(Vector3 moveDirection)
		{
			foreach (var normal in _wallCollisionNormals)
			{
				float dot = Vector3.Dot(normal, moveDirection.normalized);
				if (dot < -0.9f)
				{
					return false;
				}
			}

			return true;
		}

		#region Input Handling

		private void OnMoveUpDown(InputValue inputValue)
		{
			_forwardBackMultiplier = inputValue.Get<float>();
		}

		private void OnMoveLeftRight(InputValue inputValue)
		{
			LeftRightMultiplier = inputValue.Get<float>();
		}

		#endregion

		#region Collision Handling

		private void OnControllerColliderHit(ControllerColliderHit hit)
		{
			if (!_wallCollisionNormals.Contains(hit.normal))
			{
				_wallCollisionNormals.Add(hit.normal);
			}
		}

		#endregion

		#region Interface Functions

		public virtual EGameModeType GetGameModeType()
		{
			throw new NotImplementedException();
		}
		
		public virtual MonoBehaviour GetMonoBehaviour()
		{
			throw new NotImplementedException();
		}

		#endregion
	
		private void OnEnable()
		{
			_forcesVelocity = Vector3.zero;
			_gravitationalVelocity = MinGravityVector3;
			_slideVelocity = Vector3.zero;
		}
	}
}
