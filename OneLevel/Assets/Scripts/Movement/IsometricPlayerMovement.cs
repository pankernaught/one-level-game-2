using System;
using Camera;
using UnityEngine;

namespace Movement
{
	public class IsometricPlayerMovement : MovementAbilities
	{
		protected override Vector3 ForwardDirection => Quaternion.Euler(0f, CameraSwivel.Instance.Yaw, 0f) * Vector3.forward;
		protected override Vector3 RightDirection => Quaternion.Euler(0f, CameraSwivel.Instance.Yaw + 90f, 0f) * Vector3.forward;

		#region Interface Functions

		public override EGameModeType GetGameModeType()
		{
			return EGameModeType.Isometric;
		}

		public override MonoBehaviour GetMonoBehaviour()
		{
			return this;
		}

		#endregion
	}
}
