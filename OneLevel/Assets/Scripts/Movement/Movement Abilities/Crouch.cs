using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement.Movement_Abilities
{
    public class Crouch : MonoBehaviour
    {
        [SerializeField] protected float crouchSpeed;
        [SerializeField] protected float crouchHeight;
        [SerializeField] protected EInteractionBehaviour interactionBehaviour;
        [SerializeField] protected Transform playerBodyPivotTransform;

        //Funcs and Actions
        public Func<bool> CanCrouchFunc;
        public Action<bool> CrouchAction;
        
        private CharacterController _characterController;
        private bool _isCrouching = false;

        // Cache original settings
        private float _height;
        private Vector3 _center;
        private Vector3 _scale;

        private void Awake()
        {
            _characterController = GetComponent<CharacterController>();

            _height = _characterController.height;
            _center = _characterController.center;
            _scale = playerBodyPivotTransform.localScale;
        }

        public bool CanUnCrouch()
        {
            if (!_isCrouching) return true;
            
            LayerMask mask = LayerMask.GetMask("Landable");
            return !Physics.Raycast(transform.position + _characterController.center, Vector3.up, _height, mask);
        }

        private void OnCrouch(InputValue inputValue)
        {
            if (!CanCrouchFunc()) return;

            if (inputValue.isPressed)
            {
                if (interactionBehaviour == EInteractionBehaviour.Toggle)
                {
                    if (!CanUnCrouch()) return;
                    
                    if (_isCrouching)
                    {
                        // Release the crouch
                        ExitCrouch();
                        CrouchAction?.Invoke(_isCrouching);
                    }
                    else
                    {
                        // Engage the crouch
                        EnterCrouch();
                        CrouchAction?.Invoke(_isCrouching);
                    }
                }
                else
                {
                    // Engage the crouch
                    EnterCrouch();
                    CrouchAction?.Invoke(_isCrouching);
                }
            }
            else
            {
                if (interactionBehaviour != EInteractionBehaviour.Hold) return;
                
                // Check that we can un-crouch
                if (CanUnCrouch())
                {
                    // Release the crouch
                    ExitCrouch();
                    CrouchAction?.Invoke(_isCrouching);
                }
            }
        }

        public void EnterCrouch()
        {
            if (crouchHeight < _characterController.radius)
            {
                Debug.LogWarning("Crouch height can not be smaller that the radius. Will not shrink down to that size");
            }

            _isCrouching = true;

            float heightChangeFactor = crouchHeight / _height;

            _characterController.height = crouchHeight;
            _characterController.center = _center + Vector3.down * heightChangeFactor;

            Vector3 crouchScale = _scale;
            crouchScale.y = heightChangeFactor;
            playerBodyPivotTransform.localScale = crouchScale;

            MovementManager.Instance.CurrentMovementController.MoveSpeed = crouchSpeed;
        }

        public void ExitCrouch()
        {
            if (!_isCrouching) return;
            
            _characterController.center = _center;
            _characterController.height = _height;
            playerBodyPivotTransform.localScale = _scale;
            
            MovementManager.Instance.CurrentMovementController.ResetMoveSpeed();

            _isCrouching = false;
        }
    }
}
