using System;
using Mechanics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement.Movement_Abilities
{
	public class Jump : MonoBehaviour
	{
		[SerializeField] protected WholeCharges wholeCharges;

		[SerializeField] protected float jumpHeight;
		[Range(1f,100f)]
		[SerializeField] protected float jumpExtensionForce;
		[SerializeField] protected float jumpExtensionDelay;
		[SerializeField] protected float jumpExtensionTime;
		[SerializeField] protected float jumpBufferTime;
		[SerializeField] protected float kyoteTime;

		// Public Properties
		public bool WithinKyoteTime => _jumpsPerformed != 0 || Time.time - _lastGrounded < kyoteTime;
		
		// Funcs and Actions
		public Func<bool> CanJumpFunc;
		public Func<bool> CanJumpExtendFunc;
		public Action JumpAction;

		// Private variables
		private float _jumpExtensionMultiplier = 0f;
		private float _jumpTime = 0f;
		private float _lastJumpInput = float.MinValue;
		private float _lastGrounded;
		private Vector3 _playerFeetPos;
		private int _jumpsPerformed;

		// Constants
		private const int ChargesPerJump = 1;

		// References
		private MovementBase _movementBase = null;

		private void Awake()
		{
			GameManager.Instance.GameModeTypeChangedAction += OnGameModeTypeChanged;
		}

		private void OnGameModeTypeChanged(EGameModeType eGameModeType)
		{
			// If we have a current reference then we want to unsubscribe from the action
			if (_movementBase != null)
			{
				_movementBase.BecameGroundedAction -= OnBecameGrounded;
			}
			
			// Then get the new current reference and subscribe to the action on that controller, so we are subscribed to the 
			// active one
			_movementBase = MovementManager.Instance.CurrentMovementController;
			_movementBase.BecameGroundedAction += OnBecameGrounded;
			_movementBase.StartedFallingAction += OnStartedFalling;
			_movementBase.CanMoveAction += OnCanMove;
		}

		private void OnCanMove()
		{
			JumpBuffering();
		}

		private void OnStartedFalling()
		{
			_lastGrounded = Time.time;
		}

		private void OnBecameGrounded()
		{
			// Reset Jump Counter
			_jumpsPerformed = 0;
			
			// End jump extension
			_jumpExtensionMultiplier = 0f;
			// Regain all spent Jump charges
			wholeCharges.AddWholeCharge(wholeCharges.ChargesMax);
			
			JumpBuffering();
		}

		private void JumpBuffering()
		{
			//Jump buffer Jump
			if (Time.time - _lastJumpInput < jumpBufferTime)
			{
				DoJump();
				// Send Action to let any listeners know that we just jumped
				JumpAction?.Invoke();
			}
		}

		private void FixedUpdate()
		{
			if (CanJumpExtendFunc != null && !CanJumpExtendFunc()) return;
			
			// Add Jump Extension Force
			float jumpTimer = Time.time - _jumpTime;
			if (_jumpExtensionMultiplier > 0f && jumpTimer <= (jumpExtensionTime + jumpExtensionDelay) && jumpTimer > jumpExtensionDelay && !_movementBase.IsFalling)
			{
				_movementBase.AddVerticalVelocity(jumpExtensionForce * Time.fixedDeltaTime, ForceMode.Impulse);
			}
		}

		private void OnJump(InputValue inputValue)
		{
			if (!CanJumpFunc()) return;
			
			if (inputValue.isPressed)
			{
				DoJump();
				// Send Action to let any listeners know that we just jumped
				JumpAction?.Invoke();
			}
			else
			{
				// When we release the Jump button
				// Stop jump extension
				_jumpExtensionMultiplier = 0f;
			}
		}

        public void DoJump()
        {
	        bool canJump = wholeCharges.CanUseCharges(ChargesPerJump);
            if (canJump)
            {
	            // Use Charge
                wholeCharges.UseCharge(ChargesPerJump);

                // Apply force on the character controller for Jump
                _movementBase.AddVerticalVelocity(Mathf.Sqrt(jumpHeight * Mathf.Abs(_movementBase.GetGravity.magnitude)), ForceMode.VelocityChange);

                // Incerment Jump counter
                _jumpsPerformed++;
                
                // Jump extension
                // Reset Jump Extension Time
                _jumpTime = Time.time;
                _jumpExtensionMultiplier = 1f;
            }
            else
            {
	            // Only buffer jump when we dont have charges to jump normally
	            // Jump Buffer Timer
	            _lastJumpInput = Time.time;
            }
		}
	}
}
