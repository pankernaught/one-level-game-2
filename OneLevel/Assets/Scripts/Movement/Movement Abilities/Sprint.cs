using System;
using System.Threading.Tasks;
using Mechanics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement.Movement_Abilities
{
    public class Sprint : MonoBehaviour
    {
        [SerializeField] protected float sprintSpeed;
        [SerializeField] protected EInteractionBehaviour interactionBehaviour;
        [SerializeField] protected ContinuousCharges staminaCharges;
        [SerializeField] protected float staminaPerSecond;

        public bool IsSprinting () => _sprinting;
        
        // Funcs and Actions
        public Func<bool> CanSprint;
        public Action SprintAction;
        public Action SprintCompleteAction;
        
        // Flags
        private bool _sprinting;
        
        // References
        private MovementBase _movementBase = null;

        private void Awake()
        {
            GameManager.Instance.GameModeTypeChangedAction += OnGameModeTypeChanged;
            staminaCharges.ChargeChangedAction += OnStaminaChanged;
        }

        private void OnStaminaChanged(float currentStamina)
        {
            if (currentStamina == 0f)
            {
                ExitSprint();
                SprintCompleteAction?.Invoke();
            }
        }

        private void OnGameModeTypeChanged(EGameModeType mode)
        {
            if (_movementBase != null)
            {
                _movementBase.StoppedInputAction -= OnStoppedMoving;
            }

            _movementBase = MovementManager.Instance.CurrentMovementController;
            _movementBase.StoppedInputAction += OnStoppedMoving;
        }

        private void OnStoppedMoving()
        {
            ExitSprint();
        }
        
        private void OnSprint(InputValue inputValue)
        {
            if (inputValue.isPressed)
            {
                if (!staminaCharges.IsOnCooldown()) return;
                
                if (interactionBehaviour == EInteractionBehaviour.Toggle)
                {
                    if (!_sprinting && CanSprint())
                    {
                        EnterSprint();
                        SprintAction?.Invoke();
                    }
                    else
                    {
                        ExitSprint();
                        SprintCompleteAction?.Invoke();
                    }
                }
                else if (CanSprint())
                {
                    EnterSprint();
                    SprintAction?.Invoke();
                }
            }
            else
            {
                if (interactionBehaviour == EInteractionBehaviour.Hold)
                {
                    ExitSprint();
                    SprintCompleteAction?.Invoke();
                }
            }
        }

        public void EnterSprint()
        {
            _sprinting = true;
            staminaCharges.StartUsingCharge(staminaPerSecond);
            MovementManager.Instance.CurrentMovementController.MoveSpeed = sprintSpeed;
        }

        public void ExitSprint()
        {
            if (!_sprinting) return;
            
            _sprinting = false;
            staminaCharges.EndUsingCharge();
            MovementManager.Instance.CurrentMovementController.ResetMoveSpeed();
        }
    }
}
