using System;
using Mechanics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement.Movement_Abilities
{
	public class Dash : MonoBehaviour
	{
		[Header("Component References")]
		[SerializeField] protected WholeCharges wholeCharges;
		[SerializeField] protected MovementBase movement;
		[SerializeField] protected Material dashMaterial;
		[SerializeField] protected GameObject player;
	
		[Header("Dash Settings")]
		[SerializeField] protected float dashTime;
		[SerializeField] protected float dashDistance;
		[SerializeField] protected int chargesPerDash;

		// Proterties
		public bool Dashing() => _dashing;
		
		// Funcs and Actions
		public Func<bool> CanDashFunc;
		public Action DashAction;
		public Action DashCompleteAction;

		// Flags
		private bool _dashing = false;
		
		// Private Variables
		private float _lastDashTime;
		private Vector3 _dashSpeed;

		// Component References
		private Material _playerMaterial;
		
		// Unity Functions
		private void Start()
		{
			_playerMaterial = player.GetComponent<MeshRenderer>().material;
		}
		
		private void FixedUpdate()
		{
			if (!_dashing) return;

			// time.time is the time since play was pressed
			if (_dashing && Time.time - _lastDashTime < dashTime)
			{
				MovementBase movementBase = MovementManager.Instance.CurrentMovementController;
				_dashSpeed = movementBase.GetMoveDirection * (dashDistance / dashTime);
				movementBase.AddForce(_dashSpeed, ForceMode.VelocityChange);
			}
			else
			{
				EndDash();
			}
		}

		// Collision Handlers
		private void OnControllerColliderHit(ControllerColliderHit hit)
		{
			if (!_dashing || hit.normal.y > 0.9f) return;

			EndDash();
		}
		
		// Input Handlers
		private void OnDash(InputValue inputValue)
		{
			if (!CanDashFunc()) return;

			if (!inputValue.isPressed || _dashing) return;
			if (!wholeCharges.CanUseCharges(chargesPerDash)) return;
			
			StartDash();
		}

		private void StartDash()
		{
			_dashing = true;
			_lastDashTime = Time.time;
			
			wholeCharges.UseCharge(chargesPerDash);
			
			player.GetComponent<MeshRenderer>().material = dashMaterial;
			
			DashAction?.Invoke();
		}

		public void EndDash()
		{
			if (!_dashing) return;
			
			_dashing = false;
			
			movement.AddForce(Vector3.zero, ForceMode.VelocityChange);
			
			DashCompleteAction?.Invoke();
			
			player.GetComponent<MeshRenderer>().material = _playerMaterial;
		}
	}
}
