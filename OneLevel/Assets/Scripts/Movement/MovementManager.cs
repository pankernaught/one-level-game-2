using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Movement
{
	public class MovementManager : MonoBehaviour
	{
		public static MovementManager Instance { get; private set; } = null;
		private readonly List<MovementBase> _movementControllers = new List<MovementBase>();
		
		private void Awake()
		{
			Instance = this;

			_movementControllers.AddRange(GameObject.FindObjectsOfType<MovementBase>(true));
		}

		public MovementBase CurrentMovementController
		{
			get
			{
				foreach (var movementController in _movementControllers.Where(movementController => movementController.enabled))
				{
					return movementController;
				}

				Debug.LogError("No active MovementController!");
				return null;
			}
		}
	}
}
