using System;
using Mechanics;
using Movement.Movement_Abilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement
{
    public class LadderMovement : MovementBase
    {
        [SerializeField] private PlayerInput _playerInput;
        private InputActionMap _inputActionMap;
        private Jump _jump;

        public Ladder ActiveLadder;

        public bool MovingUp => GetMoveDirectionFromInput.y > 0f;
        protected override Vector3 ForwardDirection => Vector3.up;
        protected override Vector3 GetMoveDirectionFromInput => ForwardDirection * (_forwardBackMultiplier == 0f ? 0f : _forwardBackMultiplier > 0f ? 1f : -1f);

        public override EGameModeType GetGameModeType()
        {
            return EGameModeType.None;
        }

        protected override void Awake()
        {
            base.Awake();

            _canForces = false;
            _canSlide = false;

            _jump = GetComponent<Jump>();
        }

        private void OnEnable()
        {
            _inputActionMap = _playerInput.currentActionMap;
            _playerInput.SwitchCurrentActionMap("LadderMovement");
        }

        private void OnDisable()
        {
            _playerInput.currentActionMap = _inputActionMap;
        }

        private void OnClimbUpDown(InputValue inputValue)
        {
            _forwardBackMultiplier = inputValue.Get<float>();
        }

        private void OnLadderJump(InputValue inputValue)
        {
            ActiveLadder.ExitLadder();
            _jump.DoJump();
        }
        
        private void OnLadderDrop(InputValue inputValue)
        {
            ActiveLadder.ExitLadder();
        }
    }
}