using UnityEngine;

namespace Movement
{
	public class TwoDPlayerMovement : MovementAbilities
	{
		protected override Vector3 GetMoveDirectionFromInput => (LeftRightMultiplier * RightDirection).normalized;

		#region Interface Functions

		public override EGameModeType GetGameModeType()
		{
			return EGameModeType.TwoD;
		}

		public override MonoBehaviour GetMonoBehaviour()
		{
			return this;
		}

		#endregion
	}
}