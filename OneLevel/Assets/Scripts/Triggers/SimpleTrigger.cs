using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Triggers
{
    public class SimpleTrigger : MonoBehaviour
    {
        [SerializeField] private List<string> tags;

        public Action<Collider> TriggerEnterAction;
        public Action<Collider> TriggerExitAction;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!tags.Any(other.CompareTag)) return;
            
            TriggerEnterAction?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            TriggerExitAction?.Invoke(other);
        }
    }
}
