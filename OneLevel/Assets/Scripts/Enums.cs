public enum EInteractionBehaviour
{
    Toggle,
    Hold,
}

public enum EGameModeType
{
    None,
    Isometric,
    TwoD,
};

public enum EMovementType
{
    Isometric,
    TwoD,
    Ladder
}

public enum EMovementState
{
    Idle,
    Walking,
    Sprinting,
    Jumping,
    Falling,
    Dashing,
    Crouching,
}

public enum EStateBehaviour
{
    OnlyOnce,
    Repeatable,
}

/// <summary>
/// None: No Active State
/// Single: Only one Active Single State at a time
/// Multi: Can be active alongside Single State, single still tracked and returned to on state exit
/// Super: State Takeover, single and multi still tracked and returned to on state exit.
/// </summary>
public enum EStateType
{
    Single,
    Multi,
    Super
}

public enum EStateLocation
{
    Internal,
    External
}

public enum EStateMachineState
{
    None,   
    Single, 
    Multi,  
    Super,  
}