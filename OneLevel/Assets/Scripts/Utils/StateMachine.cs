using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Utils
{
    public class StateMachine<T> where T : System.Enum
    {
        private readonly Dictionary<T, List<StateCondition>> _canEnterState =
            new Dictionary<T, List<StateCondition>>();
        private readonly Dictionary<T, List<StateTransition>> _beforeEnterState =
            new Dictionary<T, List<StateTransition>>();
        private readonly Dictionary<T, List<StateTransition>> _beforeExitState =
            new Dictionary<T, List<StateTransition>>();
        private readonly Dictionary<T, StateLogic> _stateLogic =
            new Dictionary<T, StateLogic>();
        private readonly Dictionary<T, EStateBehaviour> _stateBehaviour =
            new Dictionary<T, EStateBehaviour>();
        private readonly Dictionary<T, EStateType> _stateType =
            new Dictionary<T, EStateType>();
        private readonly Dictionary<T, EStateLocation> _stateLocation =
            new Dictionary<T, EStateLocation>();

        
        public Action<T> MoveStateChangedAction;
        
        public delegate void StateTransition(T from, T to);
        public delegate void StateLogic();
        public delegate bool StateCondition();

        public T CurrentState()
        {
            return _hasSuper ? _superState : _hasMulti ? _multiState : _singleState;
        }

        private T _singleState;
        private T _multiState;
        private bool _hasMulti;
        private T _superState;
        private bool _hasSuper;

        private EStateType _eStateType;

        public void AddState(T state, EStateBehaviour behaviour, EStateType type, EStateLocation location,
            StateLogic stateLogic, StateCondition canEnterStateCondition,
            StateTransition beforeEnter, StateTransition beforeExit)
        {
            _stateBehaviour.Add(state, behaviour);
            _stateType.Add(state, type);
            _stateLocation.Add(state, location);
            _stateLogic.Add(state, stateLogic);
            AddCanEnterStateFunc(state, canEnterStateCondition);
            AddBeforeEnterStateLogic(state, beforeEnter);
            AddBeforeExitStateLogic(state, beforeExit);
        }

        public void ChangeMovementState(T newState)
        {
           // Debug.Log($"New state: {newState.ToString().ToUpper()}");
            
            // If the new state is actually new and we can enter the new state
            if (newState.Equals(CurrentState()) && _stateBehaviour[newState].Equals(EStateBehaviour.OnlyOnce)) return;

            EStateType newStateType = _stateType[newState];
            switch (newStateType)
            {
                case EStateType.Single:
                {
                    // These are internal stats so we need to check their Enter Condition since it's not done in other scripts
                    if (_stateLocation[newState] == EStateLocation.Internal && !CanEnterState(newState)) return;

                    if (_eStateType is EStateType.Single)
                    {
                        ExecuteTransitionFunctions(newState);
                        
                        _singleState = newState;
                        
                        // State Changed Action
                        MoveStateChangedAction?.Invoke(CurrentState());
                    }
                    else
                    {
                        _singleState = newState;
                    }

                    break;
                }
                case EStateType.Multi:
                {
                    if (_eStateType is not EStateType.Super)
                    {
                        ExecuteTransitionFunctions(newState);
                        
                        _eStateType = newStateType;
                        _multiState = newState;
                        _hasMulti = true;
                        
                        // State Changed Action
                        MoveStateChangedAction?.Invoke(CurrentState());
                    }

                    break;
                }
                case EStateType.Super:
                    ExecuteTransitionFunctions(newState);

                    _superState = newState;
                    _eStateType = newStateType;
                    _hasSuper = true;
                    
                    // State Changed Action
                    MoveStateChangedAction?.Invoke(CurrentState());
                    
                    break;
            }

            // Do the new state
            if (_stateLogic.ContainsKey(CurrentState()))
            {
                _stateLogic[CurrentState()]?.Invoke();
            }
        }

        private void ExecuteTransitionFunctions(T newState)
        {
            // If there is any before exit logic for the current state, then run it
            BeforeExit(newState);
            // If there is any before enter logic for the new state, then run it
            BeforeEnter(newState);
        }

        private void BeforeEnter(T newState)
        {
            if (!_beforeEnterState.ContainsKey(newState)) return;
            
            foreach (var logic in _beforeEnterState[newState])
            {
                logic.Invoke(CurrentState(), newState);
            }
        }

        private void BeforeExit(T newState)
        {
            if (!_beforeExitState.ContainsKey(CurrentState())) return;
            
            foreach (var logic in _beforeExitState[CurrentState()])
            {
                logic.Invoke(CurrentState(), newState);
            }
        }

        public void EndMultiState()
        {
            // If there is any before exit logic for the current state, then run it
            BeforeExit(CurrentState());
            
            _eStateType = EStateType.Single;
            _hasMulti = false;
            
            // State Changed Action
            MoveStateChangedAction?.Invoke(CurrentState());
        }

        public void EndSuperState()
        {
            // If there is any before exit logic for the current state, then run it
            BeforeExit(CurrentState());
            
            _hasSuper = false;
            if (_hasMulti)
            {
                _eStateType = EStateType.Multi;
            }
            else
            {
                _eStateType = EStateType.Single;
            }
            
            
            // State Changed Action
            MoveStateChangedAction?.Invoke(CurrentState());
        }

        private void AddCanEnterStateFunc(T state, StateCondition stateCondition)
        {
            
            AddToDictionary<Dictionary<T, List<StateCondition>>, T, StateCondition>(_canEnterState, state, stateCondition);
        }

        private void AddBeforeEnterStateLogic(T state, StateTransition stateTransition)
        {
            AddToDictionary<Dictionary<T, List<StateTransition>>, T, StateTransition>(_beforeEnterState, state, stateTransition);
        }

        private void AddBeforeExitStateLogic(T state, StateTransition stateTransition)
        {
            AddToDictionary<Dictionary<T, List<StateTransition>>, T, StateTransition>(_beforeExitState, state, stateTransition);
        }

        private void AddToDictionary<T1, T2, T3>(T1 dictionary, T2 key, T3 value) where T1 : Dictionary<T2,List<T3>>
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, new List<T3>());
            }
            
            dictionary[key].Add(value);
        }

        public bool CanEnterState(T state)
        {
            // We can enter the state if there is no func to check OR all the registered funcs return true
            return !_canEnterState.ContainsKey(state) || _canEnterState[state].All(func => func());
        }
    }
}
