using UnityEngine;
using UnityEngine.InputSystem;

public class LookBase : MonoBehaviour
{
	protected float _forwardBackLook;
	protected float _leftRightLook;
	protected float _lastTurnTime;
	protected Quaternion _currentLookDirection;
	protected Quaternion _lookDestination;

	protected readonly float ROTATION_OFFSET = -45f;

	public Quaternion GetLookDirection
	{
		get
		{
			return _currentLookDirection;
		}
	}
	protected bool HasLookInput
	{
		get
		{
			return _forwardBackLook != 0f || _leftRightLook != 0f;
		}
	}

	public Vector3 LookPoint
	{
		get
		{
			Vector3 lookPointVector = new Vector3(_forwardBackLook * 90 + transform.position.x, transform.position.y, _leftRightLook * -90 + transform.position.y);
			Vector3 pointRotated = lookPointVector - transform.position; // direction of point
			pointRotated = Quaternion.Euler(0, ROTATION_OFFSET, 0) * pointRotated; // rotate point 
			pointRotated = pointRotated + transform.position; // add back rigidbody distance point
			return pointRotated;
		}
	}

	private void Update()
	{
		_currentLookDirection = transform.rotation;
		// Had to add the mathf.abs stuff because it was turning 180 degrees when the stick snapped back into middle
		if (HasLookInput)
		{
			transform.transform.LookAt(LookPoint);
		}
	}

	private void OnLookUpDown(InputValue inputValue)
	{
		_forwardBackLook = inputValue.Get<float>();
	}

	private void OnLookLeftRight(InputValue inputValue)
	{
		_leftRightLook = inputValue.Get<float>();
	}
}
