using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

namespace Mechanics
{
	
	public class WholeCharges : MonoBehaviour
	{
		[Header("Charge Use")]
		[SerializeField] protected int maxCharges;
		[SerializeField] protected int startingCharges;
		[SerializeField] protected bool startsWithMaxCharges;
		[SerializeField] protected float minTimeBetweenChargeUse;
	
		
		[Header("Recharge Settings")]
		[SerializeField] protected bool usesRecharge = true;
		[SerializeField] protected float rechargeTime;
		[Tooltip("Async Recharge = true, Each charge has it's own timer, Async Recharge = false, Charges Recharge one after the other.")]
		[SerializeField] protected bool asyncRecharge = true;

		private int _availableCharges;
		private float _lastChargeUsedTime;
		private Task _rechargeTask = null;
		private CancellationTokenSource _cancellationToken;

		// We will trigger this action when a charge has been Recharged
		public Action<int /*Current Charges*/> RechargedAction;
		public Action ChargeUsedAction;
		
		private void Awake()
		{
			_availableCharges = startsWithMaxCharges == true ? maxCharges : startingCharges;
		}
		
		/// <summary>
		/// Return the Maximum number of Charges Configured
		/// </summary>
		public int ChargesMax => maxCharges;
		public int CurrentCharges => _availableCharges;

		/// <summary>
		/// Returns true if there is are enough Charges available to be used
		/// </summary>
		/// <param name="count">Number of Charges to check for</param>
		private bool HasChargesAvailable(int count)
		{
			bool available = _availableCharges >= count;
			return available;
		}

		/// <summary>
		/// Returns true if enough time has passed between Charge uses to make a Charge available
		/// AND there are enough Charges to use
		/// Based on 'MinTimeBetweenChargeUse'
		/// </summary>
		public bool CanUseCharges(int count)
		{
			bool coolDown = Time.time - _lastChargeUsedTime > minTimeBetweenChargeUse;
			return  coolDown && HasChargesAvailable(count);
		}

		/// <summary>
		/// Add Charges back to the Charge Pool
		/// </summary>
		/// <param name="count">The number of charges to add</param>
		public void AddWholeCharge(int count)
		{
			_availableCharges = Mathf.Min(_availableCharges + count, maxCharges);
		}

		/// <summary>
		/// Use Charges from the Charge Pool
		/// </summary>
		/// <param name="count">The number of Charges to use</param>
		public void UseCharge(int count)
		{
			if (!HasChargesAvailable(count))
			{
				Debug.LogError("Trying to use more charges than are available");
			}

			if (!CanUseCharges(count))
			{
				Debug.LogError("Not Enough time has passed between Charge Uses. See 'MinTimeBetweenChargeUse");
			}

			// Use the Charges
			_availableCharges = Mathf.Max(_availableCharges - count, 0);

			// Invoke the charge used action
			ChargeUsedAction?.Invoke();

			// Store use time to enforce 'MinTimeBetweenChargeUse' 
			_lastChargeUsedTime = Time.time;

			if (!usesRecharge) return;

			_cancellationToken = new CancellationTokenSource();
			try
			{
				if (asyncRecharge)
				{
					// Start a new Coroutine for each charge use
					RechargeAsyncWholeCharge(count, _cancellationToken.Token);
				}
				// Synchronous Recharge
				// Only start a Recharge Routine if we are not already Recharging
				else if (_rechargeTask == null || _rechargeTask.IsCompleted)
				{
					_rechargeTask = RechargeAsyncWholeCharge(1, _cancellationToken.Token);
				}
			}
			finally
			{
				_cancellationToken.Dispose();
				_cancellationToken = null;
			}
		}

		/// <summary>
		/// Recharge Async/Await method
		/// Adds a number of charges back to the pool when recharge time has elapsed
		/// </summary>
		/// <param name="count">The number of Charges to Recharge</param>
		/// <param name="token">Cancelation Token</param>
		private async Task RechargeAsyncWholeCharge(int count, CancellationToken token)
		{
			do
			{
				await Task.Delay((int) (rechargeTime * 1000), token);

				AddWholeCharge(count);

				// Trigger the RechargedAction
				RechargedAction?.Invoke(CurrentCharges);
			} while (!asyncRecharge && _availableCharges < maxCharges);
		}

		private void OnDestroy()
		{
			if (_cancellationToken != null)
			{
				_cancellationToken.Cancel();
			}
		}
	}
}
