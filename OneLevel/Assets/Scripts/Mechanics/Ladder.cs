using System;
using Movement;
using Triggers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Mechanics
{
    public class Ladder : MonoBehaviour
    {
        private SimpleTrigger _simpleTrigger;
        private bool _onLadder;

        private MovementBase _activeMovement;
        private LadderMovement _ladderMovement;
        private Collider _unit;

        private bool _inRange = false;
        private bool _hasMovedUp = false;
        private bool _climbing = false;

        private float _ladderHeight;
        
        private void Awake()
        {
            _simpleTrigger = GetComponentInChildren<SimpleTrigger>(true);
            _ladderHeight = _simpleTrigger.GetComponent<BoxCollider>().size.y;
            
            _simpleTrigger.TriggerEnterAction += OnLadderEnter;
            _simpleTrigger.TriggerExitAction += OnLadderExit;
        }

        private void FixedUpdate()
        {
            if (_inRange && !_hasMovedUp && !_climbing)
            {
                _hasMovedUp = _activeMovement.MovingUp;
                if (_hasMovedUp)
                {
                    EnterLadder(_unit);
                }
            }
        }

        private void OnBecameGrounded()
        {
            if (_hasMovedUp)
            {
                ExitLadder();
            }
        }

        private void OnLadderEnter(Collider unit)
        {
            _activeMovement = MovementManager.Instance.CurrentMovementController;
            _inRange = true;
            _unit = unit;
            
            // We are above
            if (Vector3.Distance(transform.position, unit.ClosestPoint(transform.position)) > _ladderHeight * 0.9f)
            {
                EnterLadder(unit);
            }
        }

        private void EnterLadder(Collider unit)
        {
            _climbing = true;
            
            _activeMovement.CantMoveInDirectionAction?.Invoke();
            _activeMovement.enabled = false;

            var playerMovementAbilities = _activeMovement as MovementAbilities;
            if (playerMovementAbilities != null)
            {
                playerMovementAbilities.SetCanMove(false);
            }

            _ladderMovement = unit.GetComponent<LadderMovement>();
            _ladderMovement.BecameGroundedAction += OnBecameGrounded;
            _ladderMovement.ActiveLadder = this;
            _ladderMovement.enabled = true;
        }

        private void OnLadderExit(Collider unit)
        {
            _inRange = false;
            
            ExitLadder();
        }

        public void ExitLadder()
        {
            _ladderMovement.BecameGroundedAction -= OnBecameGrounded;
            _ladderMovement.enabled = false;
            
            var playerMovementAbilities = _activeMovement as MovementAbilities;
            if (playerMovementAbilities != null)
            {
                playerMovementAbilities.SetCanMove(true);
            }
            _activeMovement.enabled = true;

            _hasMovedUp = false;
            _climbing = false;
        }
    }
}