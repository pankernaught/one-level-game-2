using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

namespace Mechanics
{
	
	public class ContinuousCharges  : MonoBehaviour
	{
		[Header("Charge Use")]
		[SerializeField] protected float maxCharges;
		[SerializeField] protected float startingCharges;
		[SerializeField] protected bool startsWithMaxCharges;
		[SerializeField] protected float minTimeBetweenChargeUse;
	
		
		[Header("Recharge Settings")]
		[SerializeField] protected bool usesRecharge = true;
		[SerializeField] protected float rechargeRate;

		private float _lastChargeUsedTime;
		private float _burnRate;
		private Task _rechargeTask = null;
		private Task _burnTask = null;
		private CancellationTokenSource _burnCancellationToken;
		private CancellationTokenSource _rechargeCancellationToken;
		
		// We will trigger this action when a charge has been Recharged
		public Action<float /*Current Charges*/> ChargeChangedAction;
		
		private void Awake()
		{
			CurrentCharges = startsWithMaxCharges == true ? maxCharges : startingCharges;
			BurningCharge = false;
		}
		
		/// <summary>
		/// Return the Maximum number of Charges Configured
		/// </summary>
		public float ChargesMax => maxCharges;
		public float CurrentCharges { get; private set; }
		public bool BurningCharge { get; private set; }

		/// <summary>
		/// Returns true if there is are enough Charges available to be used
		/// </summary>
		/// <param name="count">Number of Charges to check for</param>
		private bool HasChargesAvailable(float count)
		{
			return CurrentCharges >= count;
		}

		/// <summary>
		/// Returns true if enough time has passed between Charge uses to make a Charge available
		/// AND there are enough Charges to use
		/// Based on 'MinTimeBetweenChargeUse'
		/// </summary>
		public bool IsOnCooldown()
		{
			return Time.time - _lastChargeUsedTime > minTimeBetweenChargeUse;
		}

		/// <summary>
		/// Add Charges back to the Charge Pool
		/// </summary>
		/// <param name="count">The number of charges to add</param>
		public void AddCharge(float count)
		{
			CurrentCharges = Mathf.Min(CurrentCharges + count, maxCharges);
		}

		/// <summary>
		/// Use Charges from the Charge Pool
		/// </summary>
		/// <param name="rate">The charge burnRate</param>
		public void StartUsingCharge(float rate)
		{
			if (BurningCharge) return;
			
			if (!IsOnCooldown())
			{
				Debug.LogError("Not Enough time has passed between Charge Uses. See 'MinTimeBetweenChargeUse");
				return;
			}
			
			_burnRate = rate;
			BurningCharge = true;

			// Recharge
			// Only start a Recharge Routine if we are not already Recharging
			if (_burnTask == null || _burnTask.IsCompleted)
			{
				_burnTask = BurnChargeTask();
			}
		}

		public void EndUsingCharge()
		{
			BurningCharge = false;
			
			if (!usesRecharge) return;
			
			// Recharge
			// Only start a Recharge Routine if we are not already Recharging
			if (_rechargeTask == null || _rechargeTask.IsCompleted)
			{
				_rechargeTask = RechargeTask();
			}
		}

		private async Task BurnChargeTask()
		{
			float t = Time.time;
			
			while (BurningCharge)
			{
				await Task.Yield();

				// Use the Charges
				CurrentCharges = Mathf.Max(CurrentCharges - (Time.time - t) * _burnRate, 0);
				t = Time.time;
				
				// Invoke the charge used action
				ChargeChangedAction?.Invoke(CurrentCharges);

				if (CurrentCharges != 0f) continue;
				
				// Store use time to enforce 'MinTimeBetweenChargeUse' 
				_lastChargeUsedTime = Time.time;
				EndUsingCharge();
			}
		}
		
		
		/// <summary>
		/// Recharge Async/Await method
		/// Adds a number of charges back to the pool when recharge time has elapsed
		/// </summary>
		/// <param name="rate">The rate at which i gain back charge</param>
		private async Task RechargeTask()
		{
			float t = Time.time;
			do
			{
				await Task.Yield();

				AddCharge( (Time.time - t) * rechargeRate);
				t = Time.time;
				
				// Trigger the RechargedAction
				ChargeChangedAction?.Invoke(CurrentCharges);
			} while (!BurningCharge && CurrentCharges < maxCharges);
		}

		private void OnDestroy()
		{
			// Flipping Property will cancel any active async Task
			BurningCharge = !BurningCharge;
		}
	}
}
